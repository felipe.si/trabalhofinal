package br.ufc.quixada.cc.exec;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.ufc.quixada.cc.model.*;


public class Principal {

	List<Pessoa> pessoas = new ArrayList<>();

	Acervo acervo = new Acervo();

	List<Emprestimo> emprestimos = new ArrayList<>();

	Pessoa admin = new Funcionario("Admin", "698547", LocalDate.of(1998, 01, 14), 2.500, "admin", "admin");

	public static void main(String[] args) {  


		Principal principal = new Principal();


		Pessoa pp = new Usuario("390586", "Felipe", LocalDate.of(1998, 04, 12), "Redes");
		Pessoa pa = new Usuario("854578", "Ricardo", LocalDate.of(1999, 05, 23), "SI");
		Pessoa paa = new Usuario("478569", "William", LocalDate.of(1999, 05, 23), "CC");


		Livro l = new Livro("587", "Redes", "TI", "2011", "Tanenbaum", "Pearson");
		Livro la = new Livro("478", "Sistemas Operacionais", "TI", "2013", "Tanenbaum", "LTC");
		Livro laa = new Livro("965", "Estrutura de Dados", "TI", "2011", "Adam Drozdek", "Cengage Learning ");

		principal.pessoas.add(pp);
		principal.pessoas.add(pa);
		principal.pessoas.add(paa);
		principal.acervo.addLivro(l);
		principal.acervo.addLivro(la);
		principal.acervo.addLivro(laa);


		do {

			JTextField fieldLogin = new JTextField();
			JTextField fieldSenha = new JPasswordField();

			Object[] loginEnt = {
					"Login:", fieldLogin,
					"Senha:", fieldSenha,

			};
			int optionLogin = JOptionPane.showOptionDialog(null, loginEnt, "Preencha todos os campos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"Login", "Sair"}, "default");

			if(optionLogin == JOptionPane.INFORMATION_MESSAGE) {
				break;
			}

			else if (optionLogin == JOptionPane.OK_OPTION){
				String login = fieldLogin.getText();
				String senha = fieldSenha.getText();

				if(((Funcionario)principal.admin).getLogin().equals(login)  && ((Funcionario)principal.admin).getSenha().equals(senha)){


					String menu = "Escolha uma opção:\n\n"
							+ "1 - Cadastrar Usuario\n"
							+ "2 - Cadastrar Livro\n"
							+ "3 - Registrar Emprestimo\n"
							+ "4 - Registrar Devolucao\n"
							+ "5 - Visualizar Acervo\n"
							+ "6 - Visualizar Emprestimos\n"
							+ "7 - Remover Livro do Acervo\n"
							+ "8 - Logout\n";

					char opcao;
					do {
						opcao = JOptionPane.showInputDialog(menu).charAt(0);
						switch (opcao) {
						case '1':     

							JTextField fieldMatricula = new JTextField();
							JTextField fieldNome = new JTextField();
							JTextField fieldDate = new JTextField();
							JTextField fieldCurso = new JTextField();

							Object[] usuarioEnt = {
									"Matricula:", fieldMatricula,
									"Nome:", fieldNome,
									"Nascimento (DD/MM/YYYY) :", fieldDate,
									"Curso:", fieldCurso,
							};
							int option = JOptionPane.showConfirmDialog(null, usuarioEnt, "Preencha todos os campos", JOptionPane.OK_CANCEL_OPTION);
							if (option == JOptionPane.OK_OPTION)
							{
								String matricula = fieldMatricula.getText();
								String nome = fieldNome.getText();
								String date = fieldDate.getText();
								String curso = fieldCurso.getText();

								DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

								LocalDate localDate = LocalDate.parse(date, formatter);

								Pessoa p = new Usuario(matricula, nome, localDate, curso);
								principal.pessoas.add(p);


								JFrame frame = new JFrame("JOptionPane showMessageDialog component example");
								JOptionPane.showMessageDialog(frame, p);

							}

							break;


						case '2':     

							JTextField fieldIsbn = new JTextField();
							JTextField fieldTitulo = new JTextField();
							JTextField fieldCategoria = new JTextField();
							JTextField fieldAno = new JTextField();
							JTextField fieldAutor = new JTextField();
							JTextField fieldEditora = new JTextField();

							Object[] livro = {
									"ISBN:", fieldIsbn,
									"Titulo:", fieldTitulo,
									"Categoria:", fieldCategoria,
									"Ano:", fieldAno,
									"Autor:", fieldAutor,
									"Editora:", fieldEditora,
							};


							option = JOptionPane.showConfirmDialog(null, livro, "Preencha todos os campos", JOptionPane.OK_CANCEL_OPTION);
							if (option == JOptionPane.OK_OPTION)
							{

								String isbn = fieldIsbn.getText();
								String titulo = fieldTitulo.getText();
								String categoria = fieldCategoria.getText();
								String ano = fieldAno.getText();
								String autor = fieldAutor.getText();
								String editora = fieldEditora.getText();

								Livro l1 = new Livro(isbn, titulo, categoria, ano, autor, editora);

								JFrame frame = new JFrame("JOptionPane showMessageDialog component example");
								JOptionPane.showMessageDialog(frame, l1);

								principal.acervo.addLivro(l1);


							}



							break;

						case '3':    

							List<String> usuarioSelection = new ArrayList<>();


							for (Pessoa usuario : principal.pessoas) {
								usuarioSelection.add(usuario.getNome());
							}


							Object[] optionsUsuario = usuarioSelection.toArray();

							Object valueUsuario = JOptionPane.showInputDialog(null, 
									"Selecione o Usuario", 
									"Entrada", 
									JOptionPane.QUESTION_MESSAGE, 
									null,
									optionsUsuario, 
									optionsUsuario[0]);

							int indexUsuario = usuarioSelection.indexOf(valueUsuario);

							//System.out.println(principal.pessoas.get(indexUsuario));




							List<String> livroSelection = new ArrayList<>();

							for (Livro li : principal.acervo.getLivros()) {

								livroSelection.add(li.getTitulo());
							}

							Object[] optionsLivro = livroSelection.toArray();

							Object valueLivro = JOptionPane.showInputDialog(null, 
									"Selecione o Livro", 
									"Entrada", 
									JOptionPane.QUESTION_MESSAGE, 
									null,
									optionsLivro, 
									optionsLivro[0]);

							int indexLivro = livroSelection.indexOf(valueLivro);

							//System.out.println(principal.acervo.getLivros().get(indexLivro));




							JTextField fieldDateEmprestimo = new JTextField();
							JTextField fieldDateDevolucao = new JTextField();

							Object[] usuarioEntEmp = {
									"Data Emprestimo (DD/MM/YYYY):", fieldDateEmprestimo,
									"Data Devolucao (DD/MM/YYYY):", fieldDateDevolucao,
							};
							int optionEmp = JOptionPane.showConfirmDialog(null, usuarioEntEmp, "Preencha todos os campos", JOptionPane.OK_CANCEL_OPTION);
							if (optionEmp == JOptionPane.OK_OPTION)
							{
								String dateEmp = fieldDateEmprestimo.getText();
								String dateDev = fieldDateDevolucao.getText();

								DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

								LocalDate localDateEmp = LocalDate.parse(dateEmp, formatter);
								LocalDate localDateDev = LocalDate.parse(dateDev, formatter);

								Emprestimo emp = new Emprestimo(principal.pessoas.get(indexUsuario), principal.acervo.getLivros().get(indexLivro), localDateEmp, localDateDev);


								emp.setStatus("Emprestado");

								JFrame frame = new JFrame("JOptionPane showMessageDialog component example");
								JOptionPane.showMessageDialog(frame, emp.toString());



								principal.emprestimos.add(emp);

							}



							break;

						case '4':

							List<String> emprestimoSelection = new ArrayList<>();



							for (Emprestimo emprestimo : principal.emprestimos) {
								if(emprestimo.getStatus() == "Emprestado") {
									emprestimoSelection.add(emprestimo.toString());
								}

							}


							Object[] optionsEmprestimo = emprestimoSelection.toArray();

							Object valueEmprestimo = JOptionPane.showInputDialog(null, 
									"Selecione o Usuario", 
									"Entrada", 
									JOptionPane.QUESTION_MESSAGE, 
									null,
									optionsEmprestimo, 
									optionsEmprestimo[0]);

							int indexEmprestimo = emprestimoSelection.indexOf(valueEmprestimo);


							principal.emprestimos.get(indexEmprestimo).setStatus("Devolvido");
							//System.out.println(principal.emprestimos.get(indexEmprestimo).toString());



							break;

						case '5':

							StringBuilder builderLivros = new StringBuilder(principal.acervo.getLivros().size());

							for (Livro livroA : principal.acervo.getLivros()) {
								builderLivros.append(livroA.toString() + "\n");

							}

							JOptionPane.showMessageDialog(null, builderLivros.toString());


							break;

						case '6':


							StringBuilder builderEmp = new StringBuilder(principal.emprestimos.size());


							for (Emprestimo empA : principal.emprestimos) {
								builderEmp.append(empA.toString() + "\n");
							}

							JOptionPane.showMessageDialog(null, builderEmp.toString());

							break;


						case '7':

							List<String> removerSelection = new ArrayList<>();

							for (Livro li : principal.acervo.getLivros()) {

								removerSelection.add(li.getTitulo());
							}

							Object[] optionsRemover = removerSelection.toArray();

							Object valueRemover = JOptionPane.showInputDialog(null, 
									"Selecione o Livro", 
									"Entrada", 
									JOptionPane.QUESTION_MESSAGE, 
									null,
									optionsRemover, 
									optionsRemover[0]);

							int indexRemover = removerSelection.indexOf(valueRemover);

							//System.out.println(principal.acervo.getLivros().get(indexLivro));


							principal.acervo.removerLivro(principal.acervo.getLivros().get(indexRemover));


							break;



						case '8':
							break;


						default:
							JOptionPane.showMessageDialog(null, "Opção Inválida");
							break;
						}

					} while(opcao != '8');


				}

				else {
					System.out.println("Dados de Login incorretos");

				}
			}


		}while(true);

	}  

}