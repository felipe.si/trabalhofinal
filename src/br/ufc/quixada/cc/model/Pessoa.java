package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public abstract class Pessoa {

	private String matricula;
	private String nome;
	private LocalDate dataNasc;

	public Pessoa() {

	}

	public Pessoa(String matricula, String nome, LocalDate dataNasc) {
		this.matricula = matricula;
		this.nome = nome;
		this.dataNasc = dataNasc;
	}


	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}


	@Override
	public String toString() {
		String toStr = "";
		toStr += "Matricula: " + this.matricula + " | ";
		toStr += "Nome: " + this.nome + " | ";
		toStr += "Nascimento: " + this.dataNasc+ " | ";
		return toStr;
	}

}
