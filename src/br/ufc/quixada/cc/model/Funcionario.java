package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class Funcionario extends Pessoa {

	private double salario;
	private String login;
	private String senha;

	public Funcionario() {

	}

	public Funcionario(String nome, String matricula, LocalDate dataNasc, double salario, String login, String senha) {
		super(matricula, nome, dataNasc);
		this.salario = salario;
		this.login = login;
		this.senha = senha;
	}


	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
