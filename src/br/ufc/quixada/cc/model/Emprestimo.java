package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class Emprestimo {

	private Pessoa p;
	private Livro l;
	private LocalDate dataEmprestimo;
	private LocalDate dataDevolucao;
	private String status;

	public Emprestimo() {

	}

	public Emprestimo(Pessoa p, Livro l, LocalDate dataEmprestimo, LocalDate dataDevolucao) {
		this.p = p;
		this.l = l;
		this.dataEmprestimo = dataEmprestimo;
		this.dataDevolucao = dataDevolucao;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}



	@Override
	public String toString() {
		String toStr = "";
		toStr += "Usuario: " + this.p.getNome() + " | ";
		toStr += "Livro: " + this.l.getTitulo() + " | ";
		toStr += "Data Inicio: " + this.dataEmprestimo + " | ";
		toStr += "Data Devolucao: " + this.dataDevolucao + " | ";
		toStr += "Status: " + this.status + "\n";
		return toStr;
	}


}
