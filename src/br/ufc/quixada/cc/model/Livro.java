package br.ufc.quixada.cc.model;

public class Livro {

	private String isbn;
	private String titulo;
	private String categoria;
	private String ano;
	private String autor;
	private String editora;

	public Livro() {

	}

	public Livro(String isbn, String titulo, String categoria, String ano, String autor, String editora) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.categoria = categoria;
		this.ano = ano;
		this.autor = autor;
		this.editora = editora;
	}


	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}


	@Override
	public String toString() {
		String toStr = "";
		toStr += "ISBN: " + this.isbn + " | ";
		toStr += "Titulo: " + this.titulo + " | ";
		toStr += "Categoria: " + this.categoria + " | ";
		toStr += "Ano: " + this.ano + " | ";
		toStr += "Autor: " + this.autor + " | ";
		toStr += "Editora: " + this.editora + "\n";
		return toStr;
	}


}
