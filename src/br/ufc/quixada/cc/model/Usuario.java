package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class Usuario extends Pessoa {

	private String curso;

	public Usuario() {

	}

	public Usuario(String matricula, String nome, LocalDate dataNasc, String curso) {
		super(matricula, nome, dataNasc);
		this.curso = curso;
	}


	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}


	@Override
	public String toString() {
		String toStr = "";
		toStr += super.toString();
		toStr += "Curso: " + this.curso;
		return toStr;
	}

}
