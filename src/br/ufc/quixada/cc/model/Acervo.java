package br.ufc.quixada.cc.model;

import java.util.ArrayList;
import java.util.List;

public class Acervo implements Remocao {

	private List<Livro> livros;


	public Acervo() {
		livros = new ArrayList<>();
	}


	public void addLivro(Livro l) {
		livros.add(l);
	}

	public List<Livro> getLivros() {
		return livros;
	}


	@Override
	public void removerLivro(Livro l) {
		livros.remove(l);

	}


}
